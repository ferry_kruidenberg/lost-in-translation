# Lost in Translation

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## About the application

### *Login* 
A user enters their username to login to the application. <br/>
If an invalid username is given, an error will be shown asking to register.

### *Register*
A user can create an account by giving a username. <br/>
If a username is already taken, it will display an error message telling the user already exists.

### *Translate*
The user can input a word or sentence and the application returns this as sign language. <br/>
Special characters will be filtered out. <br/>
The user may save the translation to their profile.

### *Profile*
The user can see up to 10 saved words/sentences. <br/>
The user can clear their saved items by using the delete button. <br/>
If no items are saved, a message is given to the user.

### *Navigation bar*
While the user is logged in, buttons to go to the translation page and profile are shown. <br/>
These buttons are also in a dropdown menu, with a logout button.