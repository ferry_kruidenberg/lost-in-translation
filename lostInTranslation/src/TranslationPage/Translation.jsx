import React, {useState} from "react";
import withUser from "../hoc/withUser";
import {useUserContext} from "../context/UserContext";
import {patchUserAPI} from "../API/userAPI";
import {useTranslationContext} from "../context/TranslationContext";
import Container from "../hoc/container";

function Translation() {

    const {user} = useUserContext()
    const {translation, setTranslation} = useTranslationContext()
    const [input, setInput] = useState('')

    const handleOnChangeTranslationInput = (event) => {
        //Checks if the input is empty.
        if (event.target.value !== '') {
            //filter out special characters, since we don't have any sign images for those.
            setInput(event.target.value.toLowerCase().match(/[a-z ]+/g).join(''))
        } else {
            //If the input is empty, it will show only a space character since this is empty.
            setInput(" ")
        }
    }

    const handleSaveTranslation = async (event) => {
        let tempTranslationArray = translation.translation
        tempTranslationArray.push(input)
        //Only need to save last 10 translations, so it removes the oldest one.
        if (tempTranslationArray.length > 10) {
            tempTranslationArray = tempTranslationArray.slice(1);
        }
        setTranslation({translation: tempTranslationArray})
        await patchUserAPI(user.userId, tempTranslationArray)
    }

    return (
        <Container>
            <h2 className="mt-5">Input a word or sentence to translate</h2>
            <input placeholder="words to translate" onChange={handleOnChangeTranslationInput}/>
            <button onClick={handleSaveTranslation}>Save</button>
            <br/>
            <br/>
            {/*Uses a for-loop to display the symbols of the letters*/}
            <ul className="list-group ">
                <li className="list-group-item list-group-item-warning border border-2 border-dark liTranslation">
                    {input.split('').map(function (character, i) {
                        return (<div key={i} style={{float: 'left'}}>
                            <img className="handImage" src={"/individial_signs/" + character + ".png"} alt={
                                'logo'
                            }/>
                            {/*Use ​ (zero with space) since html ignores spaces before first non-space character.
                             Otherwise no character was set below the space image which causes layout issues.*/}
                            <p className="pTranslation">​{character}</p>
                        </div>)
                    })}
                </li>
            </ul>
        </Container>
    )
}

export default withUser(Translation)