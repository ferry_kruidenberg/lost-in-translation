import React, {useState} from "react";
import {Link, NavLink, useHistory} from "react-router-dom"
import Container from "../hoc/container";
import {useUserContext} from "../context/UserContext";
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';

export const Navbar = () => {
    const {user, setUser} = useUserContext()

    const [dropdown, setDropdown] = useState(false);
    const toggleOpen = () => setDropdown(!dropdown);

    const history = useHistory()
    const handleLogout = (event) => {
        toggleOpen();
        localStorage.removeItem("user");
        setUser({
            userId: 0,
            username: '',
            error: '',
            login: false,
        })
        history.push('/')
    }

    return (
        <nav className="navbar navbar-expand-md navbar-light bg-warning mb-3 fixed-top">
            <Container>
                <div>
                    <img className="logo" src="/Logo.png" alt="logo"/>
                    <Link className="navbar-brand" to="/translation">Lost in translation</Link>
                </div>

                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mainNavbar">
                    <span className="navbar-toggler-icon"></span>
                </button>

                {user.login &&
                <>
                    <div className="collapse navbar-collapse" id="mainNavbar">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/translation">Translate</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink className="nav-link" to="/profile">Profile</NavLink>
                            </li>
                        </ul>

                        {/*Dropdown menu for profile, translation and logout*/}
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <div className="dropdown">
                                    <button className="navbar-brand border-0 bg-transparent" onClick={toggleOpen}>
                                        <span className="material-icons align-bottom">menu</span>
                                        {user.username}
                                    </button>
                                    <div
                                        className={`dropdown-menu ${dropdown ? 'show' : ''}`}
                                        aria-labelledby="dropdownMenuButton"
                                    >
                                        <NavLink className="dropdown-item" to="/profile" onClick={toggleOpen}>
                                            Profile
                                        </NavLink>
                                        <NavLink className="dropdown-item" to="/translation" onClick={toggleOpen}>
                                            Translate
                                        </NavLink>
                                        <button className="dropdown-item" onClick={handleLogout}>
                                            Logout
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </>
                }
            </Container>
        </nav>
    )
}
