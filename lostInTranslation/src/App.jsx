import React, {useEffect} from 'react'
import './App.css'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import StartPage from "./StartPage/StartPage";
import Register from "./StartPage/Register";
import Profile from "./ProfilePage/Profile";
import Translation from "./TranslationPage/Translation";
import {Navbar} from "./Navbar/Navbar";
import {useUserContext} from "./context/UserContext";
import {fetchUserAPI} from "./API/userAPI";
import {useTranslationContext} from "./context/TranslationContext";

function App() {
    const {setUser} = useUserContext()
    const {setTranslation} = useTranslationContext()

    useEffect( () => {
        async function fetchData() {
            const localUser = JSON.parse(localStorage.getItem("user"))
            if (localUser !== null) {
                setUser({...localUser})
                const [error, result] = await fetchUserAPI(localUser.username)
                //if the user in local storage does not exist in the database, it clears the local storage and resets the user.
                if (error === null){
                    setTranslation(
                        {
                            translation: result.translations
                        })
                } else {
                    localStorage.clear()
                    setUser({
                        userId: 0,
                        login: false,
                        username: '',
                        error: ''})
                }

            }
        }

        fetchData()
    }, [setUser, setTranslation])

    return (

        <BrowserRouter>
            <div className="App">
                <Navbar/>
                <br/>
                <Switch>
                    <Route path="/" exact component={StartPage}/>
                    <Route path="/register" component={Register}/>
                    <Route path="/translation" component={Translation}/>
                    <Route path="/profile" component={Profile}/>
                </Switch>
            </div>
        </BrowserRouter>

    )
}

export default App
