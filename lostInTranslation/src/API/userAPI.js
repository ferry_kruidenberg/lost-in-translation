const apiURL = "https://trivia-api-fm.herokuapp.com"
const apiKey = "Dnk2VzRp49kgk3S2ON3y7P7RvxDBO6sZr/7DoUaCPKJ1Qd0SW1wdA+ct8ihdV8S6p2RTmijTWFsvlqkUKVShhp0="

export const fetchUserAPI = async (username) => {
    try {
        const [results] = await fetch(`${apiURL}/translations?username=${username}`)
            .then(response => response.json())
        if (results !== undefined) {
            return [null, results]
        } else {
            throw new Error("Username not found, please register")
        }
    } catch (e) {
        return [e.message, null]
    }
}

export const postUserAPI = async (username) => {
    try {
        const results = await fetch(`${apiURL}/translations`, {
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                translations: []
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not create new user')
                }
                return response.json()
            })
        return [null, results]

    } catch (e) {
        console.log(e.message)
        return [e.message, null]
    }
}

export const patchUserAPI = async (userId, translations) => {
    try {
        const results = await fetch(`${apiURL}/translations/${userId}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                translations: translations
            })
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Could not create new user')
                }
                return response.json()
            })
        return [null, results]

    } catch (e) {
        console.log(e.message)
        return [e.message, null]
    }
}


