import React from "react";
import {useTranslationContext} from "../context/TranslationContext";
import Container from "../hoc/container";
import withUser from "../hoc/withUser";
import {patchUserAPI} from "../API/userAPI";
import {useUserContext} from "../context/UserContext";

function Profile() {
    const {user} = useUserContext()
    const {translation, setTranslation} = useTranslationContext()


    const clearTranslation = async () => {
        setTranslation({
            translation: []
        })
        await patchUserAPI(user.userId, [])
    }
    return (
        <Container>
            <ul className="list-group rounded-3">
                {/*Double for-loop to loop through the translations and then loop through individual characters to display them.*/}
                {translation.translation.slice(0).reverse().map(function (string, i) {
                    return <li className="list-group-item list-group-item-warning border border-2 border-dark" key={i}>{string.split('').map(function (character, j) {
                        return (<div key={j} style={{float: 'left'}}>
                            <img className="handImage" src={"/individial_signs/" + character + ".png"} alt={character}/>
                            {/*Use ​ (zero with space) since html ignores spaces before first non-space character.
                             Otherwise no character was set below the space image which causes layout issues.*/}
                            <p className="pTranslation">​{character}</p>
                        </div>)

                    })
                    }</li>
                })
                }
            </ul>
            <br/>
            {/*Only gives a button to delete translations if there is at least one translation saved.*/}
            {translation.translation.length > 0 &&
                <button className="btn btn-danger btn-lg" onClick={clearTranslation}>Clear translations</button>
            }
            {/*Displays a message to the user if no translations are saved.*/}
            {translation.translation.length === 0 &&
                <p className="errorMsg">No translations saved</p>
            }
        </Container>
    )
}

export default withUser(Profile)