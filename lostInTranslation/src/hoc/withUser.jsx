import React, {} from "react";
import {Redirect} from "react-router-dom";

//Checks if user is logged in. Redirect appropriately
const withUser = Component => props => {
    if(localStorage.getItem("user") !== null){
        return <Component {...props}/>
    } else {
        return <Redirect to ="/"/>
    }
}

export default withUser