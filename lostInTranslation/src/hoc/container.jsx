import React from "react";

const Container = ({children}) => {
    return (
        <div className="container mb-3 mt-3">{children}</div>
    )
}

export default Container

