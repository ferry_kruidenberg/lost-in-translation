import React, {useEffect} from "react";
import {useUserContext} from "../context/UserContext";
import {Link, useHistory} from "react-router-dom";
import {fetchUserAPI, postUserAPI} from "../API/userAPI";
import Container from "../hoc/container";

function Register() {
    const {user, setUser} = useUserContext()
    const history = useHistory()


    const handleOnChangeUsername = (event) => {
        setUser({
            ...user,
            username: event.target.value
        })
    }

    //Sets error message to empty string.
    const resetError = () => {
        setUser({
            ...user,
            error: ''
        })
    }

    const checkUserExists = async () => {
        const [error] = await fetchUserAPI(user.username)
        return error === null
    }

    const handleRegister = async () => {
        if (await checkUserExists()) {
            setUser({
                ...user,
                error: "Username already exists"
            })
        } else {
            const [error, result] = await postUserAPI(user.username)
            setUser({
                ...user,
                error: error
            })
            if (error === null) {
                setUser(
                    {
                        ...user,
                        login: true,
                        userId: result.id,
                        error: error
                    })
            }
        }
    }

    const onFormSubmit = event => {
        event.preventDefault()
        handleRegister(event)
    }

    //Save the user to local storage.
    useEffect(() => {
        if (user.login !== false) {
            localStorage.setItem('user', JSON.stringify(user))
            history.push('/translation')
        }
        // eslint-disable-next-line
    }, [user.login])

    return (
        <Container>
            <form className="mt-3" onSubmit={onFormSubmit}>
                <h2>Register</h2>
                <div className="mb-3">
                    <label htmlFor="username" className="form-label">Register a username</label>
                    <input className="form-control" placeholder="username" onChange={handleOnChangeUsername}/>
                </div>
                <button type="submit" className="btn btn-primary btn-lg">Register</button>
            </form>
                <Link onClick={resetError} to="/">{"Return to login"}</Link>
                {user.error !== null &&
                <p className="errorMsg">{user.error}</p>
                }
        </Container>

    )
}

export default Register