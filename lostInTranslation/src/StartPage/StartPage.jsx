import React, {useEffect} from "react";
import {Link, useHistory} from "react-router-dom";
import {useUserContext} from "../context/UserContext";
import {fetchUserAPI} from "../API/userAPI";
import {useTranslationContext} from "../context/TranslationContext";
import Container from "../hoc/container";

function StartPage() {
    const {user, setUser} = useUserContext()
    const {setTranslation} = useTranslationContext()
    const history = useHistory()

    const handleOnChange = (event) => {
        setUser({
                ...user,
                username: event.target.value
            }
        )
    }


    //Sets error message to empty string.
    const resetError = () => {
        setUser({
            ...user,
            error: ''
        })
    }

    const handleLogin = async () => {
        //Check if username is in database
        const [error, result] = await fetchUserAPI(user.username)
        setUser({
            ...user,
            error: error
        })
        if (error === null) {
            setUser(
                {
                    ...user,
                    userId: result.id,
                    error: '',
                    login: true
                })
            setTranslation(
                {
                    translation: result.translations
                }
            )
        }
    }

    const onFormSubmit = async (event) => {
        event.preventDefault()
        await handleLogin(event)
    }

    //Save the user to local storage.
    useEffect(() => {
        if (user.login !== false) {
            localStorage.setItem('user', JSON.stringify(user))
            history.push('/translation')
        }
        // eslint-disable-next-line
    }, [user.login])

    return (
        <Container>
            <form className='mt-3' onSubmit={onFormSubmit}>
                <h1>Login</h1>
                <div className="mb-3">
                    <label htmlFor="username" className="form-label">Please enter your username</label>
                    <input className="form-control" placeholder="username" onChange={handleOnChange}/>
                </div>
                <button type="submit" className="btn btn-primary btn-lg">Login</button>
            </form>
            <Link onClick={resetError} to="/register">{"Create account"}</Link>
            {user.error !== null &&
            <p className="errorMsg">{user.error}</p>
            }
        </Container>
    )
}


export default StartPage