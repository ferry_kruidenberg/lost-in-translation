import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import UserProvider from "./context/UserContext";
import TranslationProvider from "./context/TranslationContext";
import 'bootstrap/dist/css/bootstrap.min.css'

ReactDOM.render(
  <React.StrictMode>
    <UserProvider>
        <TranslationProvider>
            <App />
        </TranslationProvider>
    </UserProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
