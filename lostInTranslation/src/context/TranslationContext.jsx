import React, {createContext, useContext, useState} from "react";

const TranslationContext = createContext(null);

export const useTranslationContext = () => {
    return useContext(TranslationContext);
}

const TranslationProvider = ({children}) => {
    const [translation, setTranslation] = useState({
        translation: []
    });

    return (
        <TranslationContext.Provider value={{translation, setTranslation}}>
            {children}
        </TranslationContext.Provider>
    )
}

export default TranslationProvider;