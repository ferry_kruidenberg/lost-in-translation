import React, {createContext, useContext, useState} from "react";

const UserContext = createContext(null);

export const useUserContext = () => {
    return useContext(UserContext);
}

const UserProvider = ({children}) => {
    const [user, setUser] = useState({
        userId: 0,
        login: false,
        username: '',
        error: ''

});

    return (
        <UserContext.Provider value={{user,setUser}}>
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider;